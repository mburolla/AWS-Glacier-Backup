#!/bin/bash
# Prerequisites: brew install jq

GLACIERVAULTNAME=<<<YOUR S3 GLACIER VAULT NAME HERE>>>
ARCHIVEID=<<<YOUR ARCHIVE ID HERE>>>
JOBID=<<<YOUR JOB ID HERE>>>

# FIRST: Create job, and record the jobId.
aws glacier initiate-job --account-id - --vault-name $GLACIERVAULTNAME --job-parameters '{"Type": "archive-retrieval", "ArchiveId": "$ARCHIVEID"}'

# SECOND: Get Status of job (Takes 3-5 hours to finish)
#aws glacier list-jobs --account-id - --vault-name $GLACIERVAULTNAME | jq '.JobList[0].JobId, .JobList[0].Completed,.JobList[0].StatusCode,.JobList[0].ArchiveId'

# FINALLY: Download your backup file.
#aws glacier get-job-output --account-id - --vault-name $GLACIERVAULTNAME --job-id $JOBID restored.zip
