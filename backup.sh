#!/bin/bash
# Prerequisites: brew install jq
# Place this file in the root directory that you wish to backup.
# Run from the command line: ./backup.sh
#

S3FILENAME=$(date +%m-%d-%Y).txt
S3BUCKETNAME=<<<YOUR S3 BUCKET NAME HERE>>>
GLACIERVAULTNAME=<<<YOUR GLACIER VAULT HERE>>>

echo "Starting Glacier backup."
echo "Creating zip file for current directory..."

zip -r -q backup.zip .

echo "Uploading to GLACIER..."
aws glacier upload-archive --account-id - --vault-name $GLACIERVAULTNAME  --body backup.zip | jq '.archiveId' > $S3FILENAME

echo "Saving archiveId for this backup to S3..."
aws s3 cp $S3FILENAME s3://$S3BUCKETNAME/$S3FILENAME

echo "Removing zip file..."
rm backup.zip

echo "Finished. Please wait 24 hours for AWS to fully consume the backup file."
