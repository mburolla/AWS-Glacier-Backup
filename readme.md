# AWS Glacier Backup and Restore
A simple project that enables you to backup/restore to/from AWS Glacier using your Mac.

# Why AWS Glacier?
It's CHEAP!!!  Currently (5/19/2018) the cost of Glacier is ONE penny for 2GB and the price drops every year. 

# How it works
The `backup.sh` and `restore.sh` scripts use the Amazon AWS CLI to upload/download zip files to AWS Glacier.
The backup script zips the current directory where the backup script resides, and saves the Glacier archive Id to a file in your S3 bucket.
After 24 hours, Amazon will fully consume your backup file into the cloud.  

Restoring your backup involves creating a restore job with Glacier.  The restore job takes 3-5 hours to complete (immediate retrieval is optional but will cost you) and returns a jobId.  
Once the restore job is finished, the backup file can be downloaded from AWS Glacier using the jobId.  The jobId is valid for 24 hours.

# Getting Started
- `brew install awscli`
- `brew install jq`
- Create an AWS Account
- Create a user account and note your AWS access key, secret key, and region
- Configure the AWS CLI with your keys AND region: `aws configure`
- Create an S3 bucket and Glacier vault using the AWS web console

# Backup
- Place the `backup.sh` script in the root directory you wish to backup
- Update the `backup.sh` script with your bucket name and vault name
- Perform a backup, execute: `./backup.sh`

# Restore
- Place the `restore.sh` in a directory you wish to save your backup file
- Look up the archiveId stored in your S3 bucket and add to the top of the restore script
- Start the restore process, execute: `./restore.sh`
- Wait 3-5 hours for the restore job to complete
- Once complete, update the restore script with the jobId
- Download your data to a file, execute: `./restore.sh`
- Unzip `restored.zip`
